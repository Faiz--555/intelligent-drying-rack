#include"airControl.h"
#include<Arduino.h>

void setFan() {
  fanOn = false;
  pinMode(MOTOR_PIN, OUTPUT);
  pinMode(FAN_BUTTON_PIN, INPUT_PULLUP);   // 设置风扇按钮为输入模式，上拉电阻
  digitalWrite(MOTOR_PIN,HIGH);
}

void fanSwitch() {
    if (digitalRead(FAN_BUTTON_PIN) == LOW) {
      fanOn = !fanOn; // 切换风扇状态
      digitalWrite(MOTOR_PIN, fanOn ? HIGH : LOW);
    }
}

void startFan(){
  fanOn = true;
  digitalWrite(MOTOR_PIN, LOW); // 风扇转动
}

void stopFan() {
  fanOn = false;
  digitalWrite(MOTOR_PIN, HIGH); // 停止风扇转动
}