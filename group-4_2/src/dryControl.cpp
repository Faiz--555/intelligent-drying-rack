#include"dryControl.h"
#include<Arduino.h>

void setHeat()
{
    heaterOn = false;
    pinMode(HEATER_PIN, OUTPUT);
    pinMode(HEATER_BUTTON_PIN, INPUT_PULLUP); // 设置热灯按钮为输入模式，上拉电阻
}

void turnOffHeater() {
  heaterOn = false;
  digitalWrite(HEATER_PIN, LOW); // 熄灭热灯
}

void startHeater(){
  heaterOn = true;
  digitalWrite(HEATER_PIN, HIGH);
}

void heatSwitch()
{
     if (digitalRead(HEATER_BUTTON_PIN) == LOW) {
      heaterOn = !heaterOn; // 切换热灯状�?
      digitalWrite(HEATER_PIN, heaterOn ? HIGH : LOW);
    }
}