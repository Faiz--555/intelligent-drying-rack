#include "Acceptsignals.h"
#include "airControl.h"
#include "dryControl.h"
#include "elevation.h"
#include "lightControl.h"
#include "reverseResistanceDetection.h"
#include "tempeartureSensor.h"
#include "uvControl.h"

DHT dht(DHTPIN, DHTTYPE);
UvcontrolLyf uncontrolLyf;
LightControl light1;
void setup()
{
    initconnect();
    setHeat();
    setFan();
    setAlarm();
    uncontrolLyf.begin();
    light1.setlight();
    light1.setup();
    initialReverseResistanceDetectionLED();
}

void loop()
{
    Acceptsignal();
    checkTemp();
    uncontrolLyf.update();
    light1.lightswitch();
    light1.loop();
    lightUpReverseResistanceDetectionLED();
}