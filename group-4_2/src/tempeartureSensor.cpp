#include"airControl.h"
#include"dryControl.h"
#include"tempeartureSensor.h"

void setAlarm() {
  Serial.begin(115200);
  dht.begin();
  pinMode(ALARM_LED_PIN, OUTPUT);
}


void blinkAlarmLED() {
  for (int i = 0; i < 5; i++) { // 报警灯闪烁5次
    digitalWrite(ALARM_LED_PIN, HIGH);
    delay(500);
    digitalWrite(ALARM_LED_PIN, LOW);
    delay(500);
  }
}

void checkTemp() {
  delay(300); // 每0.3秒读取一次温度

  float temperature = dht.readTemperature(); // 读取温度值
  Serial.print("Temperature: ");
  Serial.println(temperature);

  if (!isnan(temperature) && temperature > TEMPERATURE_THRESHOLD) {
    // 温度超过阈值，触发保护措施
    stopFan();
    turnOffHeater();
    blinkAlarmLED();
  } else {
    // 温度正常，恢复正常状态
    fanSwitch();
    heatSwitch();

    digitalWrite(ALARM_LED_PIN, LOW); // 关闭报警灯
  }
}
