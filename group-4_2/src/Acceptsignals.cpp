#include <WiFi.h>
#include"uvControl.h"
#include"elevationControl.h"
#include"dryControl.h"
#include"airControl.h"
#include"lightControl.h"
bool heaterOn;
bool fanOn;
const char* ssid = "Faizz";       // WiFi网络名称
const char* password = "72958000";   // WiFi网络密码
WiFiServer server(80);            // 使用80端口创建一个Web服务器

void initconnect() {
  Serial.begin(115200);
  delay(1000);  // 等待串口初始化完成

  // 尝试连接到WiFi网络
  Serial.println();
  Serial.print("正在连接到WiFi网络: ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi Connecting");
  Serial.print("IP地址: ");
  Serial.println(WiFi.localIP());

  // 开启Web服务器
  server.begin();
  Serial.println("服务器已启动");
}

void Acceptsignal() {
  WiFiClient client = server.available(); // 等待客户端连接

  if (client) {
    Serial.println("有新客户端连接");
    while (client.connected()) {
      if (client.available()) {
        // 读取客户端发送的数据
        String request = client.readStringUntil('\n');
        Serial.println("收到指令: " + request); // 打印收到的信号

        // 如果收到指定的命令，则执行相关操作
        if (request.indexOf("/up") != -1) {
          // 执行上升操作
          Serial.println("执行上升操作");
          // 在这里执行上升操作的代码
          motorrise();
          client.println("up"); 
          delay(100); // 等待缓冲区刷新
        }
        if (request.indexOf("/down") != -1) {
          // 执行下降操作
          Serial.println("执行下降操作");
          // 在这里执行下降操作的代码
          motordown();
           client.println("down"); 
          delay(100); // 等待缓冲区刷新
        }
        if (request.indexOf("/light/on") != -1) {
          // 执行照明操作
          Serial.println("执行照明操作");
          LightControl light1;
          light1.setlight();
          light1.trunOn();
          client.println("light/on"); 
          delay(100); // 等待缓冲区刷新
        }
        if (request.indexOf("/light/off") != -1) {
          // 执行照明操作
          Serial.println("关闭照明操作");
          LightControl light1;
          light1.setlight();
          light1.trunOff();
          client.println("light/off"); 
          delay(100); // 等待缓冲区刷新
        }
        if (request.indexOf("/uv/on") != -1) {
          // 执行紫外线杀菌操作
          Serial.println("执行紫外线杀菌操作");
          // 在这里执行紫外线杀菌操作的代码
          UvcontrolLyf flight;  //实例化对象
          UvcontrolLyf();
          flight.begin();     //初始化
          flight.turnOn();    //打开
          client.println("uv/on");
          delay(100); // 等待缓冲区刷新
        }
        if (request.indexOf("/uv/off") != -1) {
          // 执行紫外线杀菌操作
          Serial.println("关闭紫外线杀菌操作");
          // 在这里执行紫外线杀菌操作的代码
          UvcontrolLyf flight;  //实例化对象
          UvcontrolLyf();  
          flight.begin();  //初始化
          flight.turnOff(); //关闭
          client.println("uv/off");
          delay(100); // 等待缓冲区刷新
        }
        if (request.indexOf("/fan/on") != -1) {
          // 执行烘干操作
          Serial.println("执行风干操作");
          // 在这里执行烘干操作的代码
          setFan();
          startFan();
          client.println("fan/on");
          delay(100); // 等待缓冲区刷新
        }
         if (request.indexOf("/fan/off") != -1) {
          // 执行烘干操作
          Serial.println("关闭风干操作");
          // 在这里执行烘干操作的代码
           setFan();
          stopFan();
          client.println("fan/off");
          delay(100); // 等待缓冲区刷新
        }
        if (request.indexOf("/dry/on") != -1) {
          // 执行烘干操作
          Serial.println("执行烘干操作");
          // 在这里执行烘干操作的代码
          setFan();
          startFan();
          setHeat();
          startHeater();
          client.println("dry/on");
          delay(100); // 等待缓冲区刷新
        }
         if (request.indexOf("/dry/off") != -1) {
          // 执行烘干操作
          Serial.println("关闭烘干操作");
          // 在这里执行烘干操作的代码
           setFan();
          stopFan();
          setHeat();
          turnOffHeater();
          client.println("dry/off");
          delay(100); // 等待缓冲区刷新
        }
        
        client.stop(); // 关闭客户端连接
        continue;
      }
    }
    Serial.println("客户端断开连接");
  }
}