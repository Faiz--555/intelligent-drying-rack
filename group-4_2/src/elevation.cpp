#include <Stepper.h>
#include <Arduino.h>
#include <Wire.h>
#include <elevation.h>
#include <reverseResistanceDetection.h>

#define STEPS 100       // 步进电机一圈的步数
#define MOTOR_PIN_1 2   // 连接到步进电机的引脚1
#define MOTOR_PIN_2 3   // 连接到步进电机的引脚2
#define MOTOR_PIN_3 4   // 连接到步进电机的引脚3
#define MOTOR_PIN_4 5   // 连接到步进电机的引脚4
#define SWITCH_PIN1 23 // 连接到开关1的引脚
#define SWITCH_PIN2 22 // 连接到开关2的引脚

float initialWeight;//步进电机运动时的初始重量
int motorState = 0; // 记录步进电机的状态，0为不转，1为转动
int currentPosition = 0; // 记录步进电机的当前位置
// 定义开关状态
const int switchPin1 = SWITCH_PIN1; // 开关1连接的引脚
const int switchPin2 = SWITCH_PIN2; // 开关2连接的引脚
// 创建步进电机对象
Stepper stepper(STEPS, MOTOR_PIN_1, MOTOR_PIN_3, MOTOR_PIN_2, MOTOR_PIN_4);

//初始化升降按钮
void initialSwitchButton(){
  pinMode(SWITCH_PIN1, INPUT_PULLUP); // 设置升降按钮为输入上拉模式
  pinMode(SWITCH_PIN2, INPUT_PULLUP); 
  pinMode(switchPin1, INPUT_PULLUP); // 设置开关1引脚为输入模式，使用内部上拉电阻
  pinMode(switchPin2, INPUT_PULLUP); // 设置开关2引脚为输入模式，使用内部上拉电阻
}

//初始化步进电机
void initialStepperMotor(){
  // 设置步进电机的速度（可根据需要调整）
  stepper.setSpeed(150); // 50 steps per second
  int motorState = 0; // 记录步进电机的状态，0为不转，1为转动,-1为反转
  int currentPosition = 0; // 记录步进电机的当前位置
}

//将步进电机设置为静止状态
void setStepperMotorStationaryState(){
    motorState = 0;
}

//晾衣杆升降操作
void elevationOperation(int weight){
    for(int i = 0;i<100;i++){
        if (digitalRead(switchPin1) == LOW && digitalRead(switchPin2) == HIGH) { // 当开关按下时执行
            delay(300); // 稍微延时一下，防止抖动
            initialWeight = setInitialWeight(weight);//设置初始重量
            if(motorState>0){
                motorState = 1;
            }
            if(motorState<0) {
                motorState = 0;
            }
            motorState = 1-motorState; // 切换步进电机的状态，0变为1，1变为0
        }
 
        if (digitalRead(switchPin2) == LOW && digitalRead(switchPin1) == HIGH) { // 当开关按下时执行
            delay(300); // 稍微延时一下，防止抖动
            initialWeight =  setInitialWeight(weight);//设置初始重量
            if(motorState<0) {
                motorState=-1;
            }
            if(motorState > 0){
                motorState = 0;
            }
            motorState = -1-motorState; // 切换步进电机的状态，0变为1，1变为0
        }

        if (motorState != 0) { // 如果步进电机状态为1，则转动
            if(weight > initialWeight+100 || weight < initialWeight-100){
                motorState = 0;
                lightUpReverseResistanceDetectionLED();//点亮反向阻力报警灯
            }
            stepper.step(motorState);
            currentPosition += motorState;
        }
        if (currentPosition <= 0 &&  motorState == -1) {
            motorState = 0;//切换步进电机状态，0停止
            delay(200);
        } else if (currentPosition > 4500 && motorState == 1){
            motorState = 0;//切换步进电机状态，0停止
            delay(200);
        }
    }
}
void motorRise(int weight){//远程控制电机上升
    for(int i = 0;i<100;i++){
            initialWeight = setInitialWeight(weight);//设置初始重量
            if(motorState>0){
                motorState = 1;
            }
            if(motorState<0) {
                motorState = 0;
            }
            motorState = 1-motorState; // 切换步进电机的状态，0变为1，1变为0

        if (motorState != 0) { // 如果步进电机状态为1，则转动
            if(weight > initialWeight+100 || weight < initialWeight-100){
                motorState = 0;
                lightUpReverseResistanceDetectionLED();//点亮反向阻力报警灯
            }
            stepper.step(motorState);
            currentPosition += motorState;
        }
        if (currentPosition <= 0 &&  motorState == -1) {
            motorState = 0;//切换步进电机状态，0停止
            delay(200);
        } else if (currentPosition > 4500 && motorState == 1){
            motorState = 0;//切换步进电机状态，0停止
            delay(200);
        }
    }
    }

void motorDown(int weight){//远程控制电机下降
    for(int i = 0;i<100;i++){
            initialWeight =  setInitialWeight(weight);//设置初始重量
            if(motorState<0) {
                motorState=-1;
            }
            if(motorState > 0){
                motorState = 0;
            }
            motorState = -1-motorState; // 切换步进电机的状态，0变为1，1变为0
    if (motorState != 0) { // 如果步进电机状态为1，则转动
            if(weight > initialWeight+100 || weight < initialWeight-100){
                motorState = 0;
                lightUpReverseResistanceDetectionLED();//点亮反向阻力报警灯
            }
            stepper.step(motorState);
            currentPosition += motorState;
        }
        if (currentPosition <= 0 &&  motorState == -1) {
            motorState = 0;//切换步进电机状态，0停止
            delay(200);
        } else if (currentPosition > 4500 && motorState == 1){
            motorState = 0;//切换步进电机状态，0停止
            delay(200);
        }
    }
}

