
#ifndef lightControl_h
#define lightControl_h

#include <Arduino.h>
#include <FastLED.h>

// 定义LED引脚和数量
#define LIGHT_PIN 18
#define NUM_LEDS 8
#define BRIGHTNESS 32
#define COLOR_ORDER GRB

// 定义开关引脚
#define SWITCH_PIN 19

class LightControl
{
public:
    LightControl();
    // 初始化LED
    void setup();
    // 设置物理开关控制
    void loop();
    // 连接端口开启LED
    void trunOn();
    // 连接端口关闭LED
    void trunOff();

private:
    // 初始化LED数组
    CRGB *leds;

    // 初始化LED开关状态
    bool led_State;
    unsigned long lastSwitch_State;
    unsigned long debounce_Delay;

    uint8_t light_Pin;  // led引脚
    uint8_t switch_Pin; // 开关引脚
    uint8_t num_Leds;   // 数组
    uint8_t brightness; // 亮度
};

#endif