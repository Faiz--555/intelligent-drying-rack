#ifndef UVCONTROL_H
#define UVCONTROL_H

#include <Arduino.h>
#include <FastLED.h>

#define LED_PIN         0
#define NUM_LEDS        8
#define BRIGHTNESS      32
#define LED_ON_DURATION 10000 // 15分钟

#define SWITCH_PIN      17

class UvcontrolLyf {
public:
  UvcontrolLyf();

  void begin();
  void update();
  void turnOn();
  void turnOff();

private:
  uint8_t _ledPin;
  uint8_t _switchPin;
  uint8_t _numLeds;
  uint8_t _brightness;
  unsigned long _ledOnDuration;

  CRGB *_leds;
  bool _ledState;
  bool _buttonPressed;
  unsigned long _lastSwitchTime;
  unsigned long _lastDebounceTime;
  unsigned long _debounceDelay;
  unsigned long _turnOffTime;
  bool _isTimerSet;  // 新增定时器标志变量
  unsigned long _timerStartTime;  // 新增定时器开始时间变量
};

#endif