#include<Arduino.h>
#include <DHT.h>

#define DHTPIN 4      // DHT11传感器连接到ESP32的引脚4
#define DHTTYPE DHT11 // 使用DHT11传感器
#define ALARM_LED_PIN 14    // 报警灯控制引脚
#define TEMPERATURE_THRESHOLD 34 // 温度阈值，超过该值时触发保护

extern DHT dht;

void setAlarm();
void blinkAlarmLED();
void checkTemp();

