#define HEATER_PIN 12       // 热灯控制引脚
#define HEATER_BUTTON_PIN 16 // 热灯按钮引脚

extern bool heaterOn;  // 热灯状态，默认开启

void setHeat();
void startHeater();
void turnOffHeater();
void heatSwitch();