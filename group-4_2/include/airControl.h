#ifndef airControl_h
#define airControl_h
#define MOTOR_PIN 13      // 风扇直流电机控制引脚
#define FAN_BUTTON_PIN 15 // 风扇按钮引脚
extern bool fanOn;        // 风扇状态，默认开启
void setFan();
void fanSwitch();
void startFan();
void stopFan();
#endif
