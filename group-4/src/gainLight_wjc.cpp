#include <Arduino.h>
#include <hp_BH1750.h>
hp_BH1750 BH1750;

void initBH1750()
{
  // 初始化串口通信
  Serial.begin(9600);
  // 初始化光照传感器
  BH1750.begin(BH1750_TO_GROUND); // 如果地址引脚连接到VCC，请更改为BH1750_TO_VCC。
  // 校准传感器定时器
  BH1750.calibrateTiming();
}
void showLux()
{
  // 启动低质量测量
  BH1750.start(BH1750_QUALITY_LOW, 31);
  delay(1800); // 等待测量完成，根据需要进行调整
  // 获取光照强度并输出
  Serial.print(BH1750.getLux());
  Serial.print("\t");

  // 启动高质量但亮度范围受限的测量
  BH1750.start(BH1750_QUALITY_HIGH2, 254);
  delay(1800); // 等待测量完成
  // 获取光照强度并输出
  float val = BH1750.getLux();
  Serial.print(val);
  Serial.print("\t");

  // 如果传感器饱和，则根据饱和情况进行修正
  if (BH1750.saturated() == true){
    val = val * BH1750.getMtregTime() / BH1750.getTime();
  }
  Serial.println(val);
}