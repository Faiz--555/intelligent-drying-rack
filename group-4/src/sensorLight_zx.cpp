#include <Arduino.h>

const int pirPin = 2;  // HC-SR312的OUT连接到ESP32的GPIO2
const int ledPin = 4;  // LED连接到ESP32的GPIO4

void init() {
    pinMode(pirPin, INPUT);
    pinMode(ledPin, OUTPUT);
    digitalWrite(ledPin, LOW);  // 初始关闭LED
}
void motionDetection() {//运动检测
    int pirValue = digitalRead(pirPin);
    if (pirValue == HIGH) {
        // 当检测到人体移动时
        digitalWrite(ledPin, HIGH);  // 点亮LED
        delay(3000);  // 可以根据需要调整延迟时间
        //延迟3000ms
        digitalWrite(ledPin, LOW); //1s后关闭LED
    } else {
        digitalWrite(ledPin, LOW);  // 关闭LED
    }
}