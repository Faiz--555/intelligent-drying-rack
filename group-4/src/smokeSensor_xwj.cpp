#include "smokeSensor_xwj.h"

// 烟雾传感器的设置函数
void setupSmokeSensor() {   
  Serial.begin(9600);
}

// 烟雾传感器的读取和显示函数
void readAndPrintSmokeDensity() {
  int sensorValue = analogRead(smokeSensorPin); // 读取传感器值
  Serial.print("烟雾浓度（单位ppm）: ");
  Serial.println(sensorValue); // 打印传感器值
  delay(1000); // 等待1秒再次读取
}
