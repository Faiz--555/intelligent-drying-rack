#include <Arduino.h>
#include <dimmableLighting_jyt.h>

int LED_PIN = 4; // 定义LED引脚
int POT_PIN = 32; // 定义旋转电阻引脚

void begin() {

    pinMode(LED_PIN, OUTPUT);
    pinMode(POT_PIN, INPUT);
    
}

void circulate() {
    // 读取旋转电阻的值
    int potValue = analogRead(POT_PIN);

    // 映射旋转电阻的值到LED的PWM范围
    int ledBrightness = map(potValue, 0, 4095, 0, 255); // ESP32的ADC范围是0到4095

    // 设置LED的亮度
    analogWrite(LED_PIN, ledBrightness);

    // 加一点延迟
    delay(50);
    
}