#include <mulTaskProcessing_lyf.h>
#include <automaticLight_zdy.h>
#include <breathlight_hhf.h>
#include <curtainOneTouchSwitch_zx.h>
#include <dimmableLighting_jyt.h>
#include <gainLight_wjc.h>
#include <gasalarm_xwj.h>
#include <getTemperature_lyf.h>
#include <humiture_hhf.h>
#include <rainbowLight_xwj.h>
#include <sensorLight_zx.h>
#include <smokeSensor_xwj.h>
#include <smokeWarn_yyt.h>
void setup()
{
    InitMQ2(); // 烟雾告警初始化

    initSensors(); // 灯光自动控制初始化

    initCurtain(); // 窗帘一键控制初始化

    initBH1750(); // 获取光照强度信息初始化

    init(); // 人体感应灯初始化

    setupSmokeSensor(); // 天然气检测的设置函数

    setupNeoPixels(); // 七彩灯初始化

    //setupSmokeSensor(); // 烟雾传感器的设置函数

    begin(); // 声明LED无极灯引脚

    initDHT11(); // 获取温湿度信息DHT传感器初始化

    taskSet(); // ESP32实现多任务处理初始化
}

void loop()
{
    showWarn(); // 烟雾告警

    controlLights(); // 灯光自动控制

    curtainControl(); // 窗帘控制

    showLux(); // 获取并显示光照强度信息

    motionDetection(); // 人体运动感应

    triggerAlarm();    // 蜂鸣器警报触发函数
    checkSmokeLevel(); // 监测气体泄漏函数

    readAndPrintSmokeDensity(); // 烟雾传感器的读取和显示函数

    circulate(); // 设置LED无极灯旋转电阻

    Show(); // 显示温湿度信息

    taskLoop(); // 循环检查任务完成
}