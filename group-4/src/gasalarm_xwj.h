#ifndef gasalarm_xwj_h
#define gasalarm_xwj_h

#include <Arduino.h>

#define MQ2_PIN  36 // MQ2传感器连接的模拟引脚
#define BUZZER_PIN 12  // 蜂鸣器连接的数字引脚

void setupSmokeSensor(); // 初始化函数
void triggerAlarm(); // 蜂鸣器警报触发函数
void checkSmokeLevel(); // 监测气体泄漏函数

#endif
