#include <Arduino.h>

TaskHandle_t Task1; // Task 1的句柄
TaskHandle_t Task2; // Task 2的句柄

volatile bool task1Complete = false; // Task 1完成标志
volatile bool task2Complete = false; // Task 2完成标志

void Task1code( void * parameter ) // Task 1的函数
{
  for(;;)
  {
    Serial.print("task 1 is operating...\n"); // 打印任务1正在运行的消息
    delay(1000); // 等待1秒
    task1Complete = true; // 设置任务1完成标志位
  }
}

void Task2code( void * parameter ) // Task 2的函数
{
  for(;;)
  {
    Serial.print("task 2 is operating...\n"); // 打印任务2正在运行的消息
    delay(1000); // 等待1秒
    task2Complete = true; // 设置任务2完成标志位
  }
}

void taskSet() { // 设置任务
  Serial.begin(115200); // 初始化串口通信

  // 创建任务1
  xTaskCreate(
    Task1code,       // Task 1的函数
    "Task1",         // Task 1的名称
    10000,           // Task 1的堆栈大小
    NULL,            // Task 1的参数
    1,               // Task 1的优先级
    &Task1);         // Task 1的句柄

  // 创建任务2
  xTaskCreate(
    Task2code,       // Task 2的函数
    "Task2",         // Task 2的名称
    10000,           // Task 2的堆栈大小
    NULL,            // Task 2的参数
    1,               // Task 2的优先级
    &Task2);         // Task 2的句柄
}

void taskLoop() { // 循环任务
  // 检查任务完成标志位
  if (task1Complete) {
    Serial.println("Task 1 has completed."); // 打印任务1完成的消息
    task1Complete = false; // 重置任务1完成标志位
  }

  if (task2Complete) {
    Serial.println("Task 2 has completed."); // 打印任务2完成的消息
    task2Complete = false; // 重置任务2完成标志位
  }

}