#include <Adafruit_NeoPixel.h>

// 定义 LED 引脚和数量
#define LED_PIN_1 5   // 第一个Neopixel连接到ESP32的GPIO5
#define LED_COUNT_1 8 // 第一个Neopixel的数量

#define LED_PIN_2 18  // 第二个Neopixel连接到ESP32的GPIO18
#define LED_COUNT_2 8 // 第二个Neopixel的数量

// 定义任务句柄
extern TaskHandle_t Task1;
extern TaskHandle_t Task2;
// 定义第一个 Neopixel 对象
Adafruit_NeoPixel strip1(LED_COUNT_1, LED_PIN_1, NEO_GRB + NEO_KHZ800);

// 定义第二个 Neopixel 对象
Adafruit_NeoPixel strip2(LED_COUNT_2, LED_PIN_2, NEO_GRB + NEO_KHZ800);

// 定义任务句柄
TaskHandle_t Task1;
TaskHandle_t Task2;
void rainbowCycle(void *pvParameters);
// 初始化 NeoPixel 和创建任务   调用该函数完成初始化
void setupNeoPixels()
{
  strip1.begin();
  strip2.begin();

  xTaskCreatePinnedToCore(rainbowCycle, "Task1", 4096, &strip1, 1, &Task1, 1);
  xTaskCreatePinnedToCore(rainbowCycle, "Task2", 4096, &strip2, 1, &Task2, 1);
}

// 获取 Wheel 颜色
uint32_t Wheel(byte WheelPos)
{
  if (WheelPos < 85)
  {
    return strip1.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  }
  else if (WheelPos < 170)
  {
    WheelPos -= 85;
    return strip1.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  else
  {
    WheelPos -= 170;
    return strip1.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}

// 彩虹循环任务
void rainbowCycle(void *pvParameters)
{
  Adafruit_NeoPixel *strip = reinterpret_cast<Adafruit_NeoPixel *>(pvParameters);
  uint16_t i, j;
  while (true)
  {
    for (j = 0; j < 256 * 5; j++)
    { // 5 次完整的彩虹循环
      for (i = 0; i < strip->numPixels(); i++)
      {
        strip->setPixelColor(i, Wheel(((i * 256 / strip->numPixels()) + j) & 255));
      }
      strip->show();
      vTaskDelay(20 / portTICK_PERIOD_MS); // 延时 20 毫秒
    }
  }
}
