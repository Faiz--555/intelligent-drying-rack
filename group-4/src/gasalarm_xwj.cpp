#include "gasalarm_xwj.h"

// 烟雾传感器和蜂鸣器设置函数   调用该函数完成初始化
void setupSmokeSensor() {
  Serial.begin(9600);
  pinMode(MQ2_PIN, INPUT);
  pinMode(BUZZER_PIN, OUTPUT);
  digitalWrite(BUZZER_PIN, HIGH); // 初始化蜂鸣器为高电平，关闭状态
}

// 蜂鸣器报警函数
void triggerAlarm() {
  digitalWrite(BUZZER_PIN, LOW);  // 打开蜂鸣器
  delay(1000);  // 警报持续1秒
  digitalWrite(BUZZER_PIN, HIGH);  // 关闭蜂鸣器
}

// 监测气体泄漏函数   调用该函数进行气体泄漏监测
void checkSmokeLevel() {
  int smokeLevel = analogRead(MQ2_PIN);  // 读取烟雾水平

  Serial.print("Smoke Level（ppm）: ");
  Serial.println(smokeLevel);

  // 如果检测到烟雾浓度大于100ppm，触发蜂鸣器
  if (smokeLevel > 100) {
    Serial.println("Smoke detected!");
    triggerAlarm();
    delay(1000);  // 避免持续触发警报
  }

  delay(1000);  // 每秒检测一次
}

