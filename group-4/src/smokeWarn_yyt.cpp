#include <Arduino.h>
#define SMOKE_PIN 36 // MQ2的D0连接到ESP32的GPI36
#define BUZZER_PIN 3 // 蜂鸣器的IO接口连接到ESP32的GPIO3
void InitMQ2()
{
    Serial.begin(9600);
    pinMode(SMOKE_PIN, INPUT);      // 初始化MQ2为
    pinMode(BUZZER_PIN, OUTPUT);    // 初始化蜂鸣器为输出
    digitalWrite(BUZZER_PIN, HIGH); // 初始化蜂鸣器为高电平，关闭状态
}

void showWarn()
{
    int smokeThreshold = 1000;
    int smokeValue = analogRead(SMOKE_PIN);
    Serial.print("Smoke Value: ");
    Serial.println(smokeValue); // 打印烟雾浓度值到串口
    if (smokeValue > smokeThreshold)
    {
        digitalWrite(BUZZER_PIN, LOW);  // 触发报警
        delay(1000);                    // 报警持续1秒
        digitalWrite(BUZZER_PIN, HIGH); // 关闭状态
    }
    else
    {

        delay(1000); // 延迟一秒钟再次读取烟雾浓度值
    }
}