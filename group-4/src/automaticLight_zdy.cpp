#include <Arduino.h>
#include <Wire.h> // ���� I2C ��

// ���������Ӧģ�����ӵ�����
#define PIR_PIN 2
// ����LED���ӵ�����
#define LED_PIN 13

// ���������ֵ
#define LIGHT_THRESHOLD 50

void initSensors()
{
  // ��ʼ������ͨ��
  Serial.begin(9600);
  // ����PIR_PINΪ����
  pinMode(PIR_PIN, INPUT);
  // ����LED_PINΪ���
  pinMode(LED_PIN, OUTPUT);

  // ��ʼ�� I2C ����
  Wire.begin();
}

void controlLights()
{
  // ��ȡPIR_PIN���ŵ�״̬
  int pirState = digitalRead(PIR_PIN);

  // ��ȡ������������ֵ
  Wire.beginTransmission(0x23); // GY-302 I2C ��ַ
  Wire.write(0x10);             // ���ƼĴ�����ַ
  Wire.endTransmission();
  Wire.requestFrom(0x23, 2);                             // ��ȡ 2 ���ֽڵ�����
  int lightIntensity = (Wire.read() << 8) | Wire.read(); // ��װ����

  // �������ǿ�ȵ�����ֵ���Ҽ�⵽�����˶�
  if (lightIntensity < LIGHT_THRESHOLD && pirState == HIGH)
  {
    Serial.println("Motion detected! Turning on the light.");
    digitalWrite(LED_PIN, HIGH); // ��LED��
  }
  else
  {
    Serial.println("No motion detected or light intensity too high. Turning off the light.");
    digitalWrite(LED_PIN, LOW); // �ر�LED��
  }

  delay(500); // �ӳ�һ��ʱ���ٽ�����һ�μ��
}
