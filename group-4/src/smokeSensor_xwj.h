#ifndef smokeSensor_xwj_h
#define smokeSensor_xwj_h

#include <Arduino.h>

const int smokeSensorPin = 36; // 使用 ADC1_CH0，对应引脚号 36

void setupSmokeSensor(); // 烟雾传感器的设置函数
void readAndPrintSmokeDensity(); // 烟雾传感器的读取和显示函数

#endif
