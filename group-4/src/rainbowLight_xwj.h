#ifndef rainbowLight_xwj_h
#define rainbowLight_xwj_h

#include <Adafruit_NeoPixel.h>

// 定义 LED 引脚和数量
#define LED_PIN_1    5  // 第一个Neopixel连接到ESP32的GPIO5
#define LED_COUNT_1  8  // 第一个Neopixel的数量

#define LED_PIN_2    18 // 第二个Neopixel连接到ESP32的GPIO18
#define LED_COUNT_2  8  // 第二个Neopixel的数量

// 定义任务句柄
extern TaskHandle_t Task1;
extern TaskHandle_t Task2;

// 声明初始化 NeoPixel 的函数
void setupNeoPixels();

// 声明获取 Wheel 颜色的函数
uint32_t Wheel(byte WheelPos);

// 声明彩虹循环的函数
void rainbowCycle(void *pvParameters);

#endif
