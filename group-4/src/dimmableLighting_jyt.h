#ifndef dimmableLighting_jyt_h
#define dimmableLighting_jyt_h
#include <Arduino.h>

extern int LED_PIN; // 声明LED引脚
extern int POT_PIN; // 声明旋转电阻引脚

void begin();
void circulate();

#endif