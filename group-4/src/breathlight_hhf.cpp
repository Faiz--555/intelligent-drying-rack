#include <Arduino.h>

const int ledPin = 2; // 内置LED连接的引脚号

void initbreathlight() {
  pinMode(ledPin, OUTPUT); // 设置LED引脚为输出模式
}

void showbreathlight() {
  // 逐渐增加LED亮度
  for (int brightness = 0; brightness <= 255; brightness++) {
    analogWrite(ledPin, brightness); // 设置LED亮度
    delay(10); // 延迟一段时间，调整呼吸速度
  }
  
  // 逐渐减小LED亮度
  for (int brightness = 255; brightness >= 0; brightness--) {
    analogWrite(ledPin, brightness); // 设置LED亮度
    delay(10); // 延迟一段时间，调整呼吸速度
  }
}