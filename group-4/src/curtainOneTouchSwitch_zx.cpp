#include <Arduino.h>
#include <Stepper.h>

const int stepsPerRevolution =600; // 更改此值以适应每圈的步数

// ULN2003 电机驱动模块的引脚
#define IN1 2
#define IN2 4
#define IN3 5
#define IN4 15
// 初始化步进电机库
Stepper myStepper(stepsPerRevolution, IN1, IN3, IN2, IN4);

// 开关引脚
const int switchPin = 18;

// 电机当前方向
int motorDirection = 1; // 1 表示顺时针，-1 表示逆时针
bool motorActivated = false; // 电机是否已激活

void initCurtain() {
// 设置速度为每分钟5转
 myStepper.setSpeed(50);

// 初始化串口通信
 Serial.begin(115200);

// 初始化开关引脚
 pinMode(switchPin, INPUT_PULLUP);
}

void curtainControl() {
// 读取开关状态
 int switchState = digitalRead(switchPin);

// 检测开关状态变化
 if (switchState == LOW && !motorActivated) {
  motorDirection *= -1; // 切换方向
  delay(200); // 延时消除抖动

// 激活电机
  motorActivated = true;
 }

// 控制电机摆动
 if (motorActivated) {
  if (motorDirection == 1) {
   Serial.println("clockwise");
   myStepper.step(stepsPerRevolution);
  } else {
   Serial.println("counterclockwise");
   myStepper.step(-stepsPerRevolution);
  }

// 停止电机
  myStepper.step(0);
  motorActivated = false;
 }
}