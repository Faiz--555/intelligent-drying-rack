#ifndef elevationControl
#define elevationControl

void initialSwitchButton();//初始化升降按钮
void initialStepperMotor();//初始化步进电机
void setStepperMotorStationaryState();//将步进电机设置为静止状态
void elevationOperation(int weight);//晾衣杆升降操作
void motorRise(int weight);//远程控制电机上升
void motorDown(int weight);//远程控制电机下降

#endif