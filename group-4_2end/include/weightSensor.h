#ifndef weightSensor_h
#define weightSensor_h

void initialDetetion_and_Elevation();//初始化重力检测器、与重力相关的报警装置和步进电机
void loopDetetion_and_Elevation();//检测是否超重、是否遇到反向阻力和控制上升下降
void overweightLock_and_Alarm();//超重时锁定升降按钮并报警
int loopDetetion_and_Elevation_Rise();//用于远程控制的检测重量、反向阻力和升操作
int loopDetetion_and_Elevation_Down();//用于远程控制的检测重量、反向阻力和降操作
void Unlocking_and_Alarm();//不超重时解除锁定与解除报警

#endif