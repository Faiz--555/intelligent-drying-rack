#define MOTOR_PIN 33        // 风扇直流电机控制引脚
#define FAN_BUTTON_PIN 3  // 风扇按钮引脚
extern bool fanOn;     // 风扇状态，默认开启
void setFan();     // 风扇初始化函数
void fanSwitch();  // 风扇开关函数
void startFan();   // 风扇开启函数
void stopFan();    // 风扇关闭函数