#ifndef reverseResistanceDetection_h
#define reverseResistanceDetection_h

void initialReverseResistanceDetectionLED();//初始化反向阻力报警灯
float setInitialWeight(int weight);//设置步进电机启动前的初始重量
void lightUpReverseResistanceDetectionLED();//点亮反向阻力报警灯

#endif