// #ifndef UVCONTROL_H
// #define UVCONTROL_H

// #include <Arduino.h>
// #include <FastLED.h>

// // 定义 LED 引脚和数量
// #define LED_PIN     0
// #define NUM_LEDS    8
// #define BRIGHTNESS  32
// #define LED_ON_DURATION 900000 // 15分钟

// // 定义开关引脚
// #define SWITCH_PIN  17

// class UvcontrolLyf {
// public:
//   UvcontrolLyf();

//   void begin();
//   void update();
//   void turnOn();
//   void turnOff();

// private:
//   uint8_t _ledPin;
//   uint8_t _switchPin;
//   uint8_t _numLeds;
//   uint8_t _brightness;
//   unsigned long _ledOnDuration;

//   CRGB *_leds;
//   bool _ledState;
//   unsigned long _lastSwitchTime;
//   unsigned long _lastDebounceTime;
//   unsigned long _debounceDelay;
//   unsigned long _turnOffTime; // 记录要关闭灯的时间
// };

// #endif


// #ifndef UVCONTROL_H
// #define UVCONTROL_H

// #include <Arduino.h>
// #include <FastLED.h>

// // 定义 LED 引脚和数量
// #define LED_PIN     0
// #define NUM_LEDS    8
// #define BRIGHTNESS  32
// #define LED_ON_DURATION 900000 // 15分钟

// // 定义开关引脚
// #define SWITCH_PIN  17

// class UvcontrolLyf {
// public:
//   UvcontrolLyf();

//   void begin();
//   void update();
//   void turnOn();
//   void turnOff();

// private:
//   uint8_t _ledPin;
//   uint8_t _switchPin;
//   uint8_t _numLeds;
//   uint8_t _brightness;
//   unsigned long _ledOnDuration;

//   CRGB *_leds;
//   bool _ledState;
//   bool _buttonPressed; // 添加按钮按下状态变量
//   unsigned long _lastSwitchTime;
//   unsigned long _lastDebounceTime;
//   unsigned long _debounceDelay;
//   unsigned long _turnOffTime; // 记录要关闭灯的时间
// };

// #endif

// uvControl.h
#ifndef UVCONTROL_H
#define UVCONTROL_H

#include <Arduino.h>
#include <FastLED.h>

#define LED_PIN         32
#define NUM_LEDS        8
#define BRIGHTNESS      32
#define LED_ON_DURATION 10000 // 15分钟

#define SWITCH_PIN      17

class UvcontrolLyf {
public:
  UvcontrolLyf();

  void begin();
  void update();
  void turnOn();
  void turnOff();

private:
  uint8_t _ledPin;
  uint8_t _switchPin;
  uint8_t _numLeds;
  uint8_t _brightness;
  unsigned long _ledOnDuration;

  CRGB *_leds;
  bool _ledState;
  bool _buttonPressed;
  unsigned long _lastSwitchTime;
  unsigned long _lastDebounceTime;
  unsigned long _debounceDelay;
  unsigned long _turnOffTime;
  bool _isTimerSet;  // 新增定时器标志变量
  unsigned long _timerStartTime;  // 新增定时器开始时间变量
};

#endif