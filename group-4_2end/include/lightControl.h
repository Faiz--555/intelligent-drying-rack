#ifndef LIGHTCONTROL_H
#define LIGHTCONTROL_H

#include <Arduino.h>
#include <FastLED.h>

// 定义LED引脚和数量
#define LIGHT_PIN 18
#define NUM_LEDS 8
#define BRIGHTNESS 32
#define COLOR_ORDER GRB

// 定义开关引脚
#define SWITCH_PIN 19

class LightControl
{
public:
    LightControl();
    void setlight();
    void lightswitch();
    void trunOn();
    void trunOff();

private:
    // 初始化LED数组
    CRGB *leds;

    // 初始化LED开关状态
    bool led_State;
    unsigned long lastSwitch_State;
    unsigned long debounce_Delay;


    uint8_t light_Pin;
    uint8_t switch_Pin;
    uint8_t num_Leds;
    uint8_t brightness;

};

#endif