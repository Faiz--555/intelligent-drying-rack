#include <Arduino.h>
#include <reverseResistanceDetection.h>

#define R_LED_PIN 32 // 反向阻力警报LED引脚

//初始化反向阻力报警灯
void initialReverseResistanceDetectionLED(){
    pinMode(R_LED_PIN, OUTPUT); // 设置反向阻力警报LED引脚为输出模式
    digitalWrite(R_LED_PIN,LOW);//初始状态下关闭反向阻力警报LED
}

//设置步进电机启动前的初始重量
float setInitialWeight(int weight){
    return weight;
}

//点亮反向阻力报警灯
void lightUpReverseResistanceDetectionLED(){
    digitalWrite(R_LED_PIN, HIGH); // LED亮起
    delay(5000); // 延迟5000毫秒（即5秒）
    digitalWrite(R_LED_PIN, LOW); // LED熄灭
}

