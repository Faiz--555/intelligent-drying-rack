#include <HX711.h>
#include <Wire.h>
#include <weightSensor.h>
#include <elevation.h>
#include <reverseResistanceDetection.h>

#define GD_LED_PIN 27 // 重力传感器警报LED引脚
#define DT_PIN  26 // HX711 DT引脚
#define SCK_PIN 25// HX711 SCK引脚
#define weightThreshold 3000.0f //重量阈值为3kg

HX711 scale; // 创建对象

bool isOverweight=false;//标记是否超重
bool buttonLocked=false;//标记是否锁定升降按钮
unsigned long lastAlertTime = 0; // 记录上次报警时间

//初始化重力检测器、与重力相关的报警装置和步进电机
void initialDetetion_and_Elevation(){
  pinMode(GD_LED_PIN, OUTPUT); // 设置重力传感器警报LED引脚为输出模式
  digitalWrite(GD_LED_PIN,LOW);//初始状态下关闭重力传感器警报LED
  initialReverseResistanceDetectionLED();//初始化反向阻力报警灯
  scale.begin(DT_PIN,SCK_PIN);//调用scale对象的begin方法，传入先前定义的引脚编号，初始化HX711传感器与ESP32的连接。
  //默认，通道A，128倍增益
    scale.read();
    scale.read_average(20);
    scale.get_value(5);
    scale.get_units(5);
    scale.set_scale(666.f);//比例因子设置
    scale.tare();//调用scale对象的tare方法，将秤设置为零点（皮重），后续读数将减去这个皮重值。
    scale.read();
    scale.read_average(20);
    scale.get_value(5);
    scale.get_units(5);
    initialSwitchButton();//初始化升降按钮
    initialStepperMotor();//初始化步进电机
}


//检测是否超重、是否遇到反向阻力和控制上升下降
void loopDetetion_and_Elevation(){
  float weight = (scale.get_units());//调用scale对象的read方法，获取HX711传感器的当前重量读数，存储在float类型的变量weight中。
  if(weight>weightThreshold){
    overweightLock_and_Alarm();//超重时锁定升降按钮并报警
  }else{
    Unlocking_and_Alarm();//不超重时解除锁定与解除报警
  }
  if(!buttonLocked){
    //晾衣杆升降操作
    elevationOperation(weight);
  }
}


//用于远程控制的检测重量、反向阻力和升操作
int loopDetetion_and_Elevation_Rise(){
  float weight = (scale.get_units());//调用scale对象的read方法，获取HX711传感器的当前重量读数，存储在float类型的变量weight中。
  if(weight>weightThreshold){
    overweightLock_and_Alarm();//超重时锁定升降按钮并报警
  }else{
    Unlocking_and_Alarm();//不超重时解除锁定与解除报警
  }
  int tmp=1;
  if(!buttonLocked){
    //远程控制晾衣杆升操作
   tmp= motorRise(weight);
  }
  return tmp;
}


//用于远程控制的检测重量、反向阻力和降操作
int loopDetetion_and_Elevation_Down(){
  float weight = (scale.get_units());//调用scale对象的read方法，获取HX711传感器的当前重量读数，存储在float类型的变量weight中。
  if(weight>weightThreshold){
    overweightLock_and_Alarm();//超重时锁定升降按钮并报警
  }else{
    Unlocking_and_Alarm();//不超重时解除锁定与解除报警
  }
  int tmp=1;
  if(!buttonLocked){
    //远程控制晾衣杆降操作
    tmp=motorDown(weight);
  }
  return tmp;
}


//超重时锁定升降按钮并报警
void overweightLock_and_Alarm(){
  setStepperMotorStationaryState();//将步进电机设置为静止状态
  if(!isOverweight){
    //首次超重，触发报警并锁定按钮
    buttonLocked=true;
    lastAlertTime=millis();//记录当前时间为首次报警时间
  }
  isOverweight=true;
  unsigned long currentTime=millis();
  if ((currentTime-lastAlertTime)>=100){//每隔0.1秒闪烁一次LED
    lastAlertTime=currentTime;
    digitalWrite(GD_LED_PIN,!digitalRead(GD_LED_PIN));//反转LED状态
  } 
}   


//不超重时解除锁定与解除报警
void Unlocking_and_Alarm(){
  if(isOverweight){
  //之后超重状态解除
    digitalWrite(GD_LED_PIN,LOW);//关闭超重报警灯
    buttonLocked=false;
  }
  isOverweight=false;
}
