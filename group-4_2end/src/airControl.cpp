#include"airControl.h"
#include<Arduino.h>

// 风扇初始化函数
void setFan() {
  fanOn = false;
  pinMode(MOTOR_PIN, OUTPUT);
  pinMode(FAN_BUTTON_PIN, INPUT_PULLUP);   // 设置风扇按钮为输入模式，上拉电阻
  digitalWrite(MOTOR_PIN,HIGH);
}

// 风扇开关函数
void fanSwitch() {
    if (digitalRead(FAN_BUTTON_PIN) == LOW) {
      fanOn = !fanOn; // 切换风扇状态
      digitalWrite(MOTOR_PIN, fanOn ? HIGH : LOW);
    }
}

// 风扇开启函数
void startFan(){
  fanOn = true;
  digitalWrite(MOTOR_PIN, LOW); // 风扇转动
}

// 风扇关闭函数
void stopFan() {
  fanOn = false;
  digitalWrite(MOTOR_PIN, HIGH); // 停止风扇转动
}