// #include <Arduino.h>
// #include <FastLED.h>

// // 定义LED引脚和数量
// #define LED_PIN     0
// #define NUM_LEDS    8
// #define BRIGHTNESS  32
// #define LED_TYPE    WS2812
// #define COLOR_ORDER GRB

// // 定义开关引脚
// #define SWITCH_PIN  17

// // 初始化LED数组
// CRGB leds[NUM_LEDS];

// // 初始化LED开关状态
// bool ledState = false;

// // 上次切换LED状态的时间
// unsigned long lastSwitchTime = 0;
// // 定义LED持续亮的时间，900000ms代表15分钟
// unsigned long ledOnDuration = 900000;

// // 上次读取到开关为LOW的时间
// unsigned long lastDebounceTime = 0;
// // 防抖时间
// unsigned long debounceDelay = 50;

// void setup() {
//   // 初始化LED
//   FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
//   FastLED.setBrightness(BRIGHTNESS);

//   // 初始化开关引脚
//   pinMode(SWITCH_PIN, INPUT_PULLUP);
// }

// void loop() {
//   // 读取开关状态
//   int switchState = digitalRead(SWITCH_PIN);

//   // 检查是否需要防抖
//   if (switchState == LOW) {

//     // 更新最后一次开关被按下为LOW的时间
//     if ((millis() - lastDebounceTime) > debounceDelay) {
//       lastDebounceTime = millis();

//       // 切换LED状态
//       if (!ledState) { // 如果LED当前是关闭的，打开它
//         ledState = true;
//         lastSwitchTime = millis(); // 记录开灯的时间
//         for (int i = 0; i < NUM_LEDS; i++) {
//           leds[i] = CRGB::Purple; // 设置为紫色
//         }
//         FastLED.show(); // 更新LED显示
//             delay(200);
//       } else { // 如果LED已经开启，关闭它
//         ledState = false;
//         for (int i = 0; i < NUM_LEDS; i++) {
//           leds[i] = CRGB::Black; // 设置为黑色
//         }
//         FastLED.show();
//             delay(200);
//       }
//     }


//   }

//   // 如果LED已经开启，并且开启时间超过了10秒，自动关闭LED
//   if (ledState && (millis() - lastSwitchTime > ledOnDuration)) {
//     ledState = false; // 更新状态为关闭
//     for (int i = 0; i < NUM_LEDS; i++) {
//       leds[i] = CRGB::Black; // 设置为黑色
//     }
//     FastLED.show();
//   }
// }

// #include "uvControl.h"

// UvcontrolLyf::UvcontrolLyf() {
//   _ledPin = LED_PIN;
//   _switchPin = SWITCH_PIN;
//   _numLeds = NUM_LEDS;
//   _brightness = BRIGHTNESS;
//   _ledOnDuration = LED_ON_DURATION;

//   _leds = new CRGB[_numLeds];
//   _ledState = false;
//   _lastSwitchTime = 0;
//   _lastDebounceTime = 0;
//   _debounceDelay = 50;
//   _turnOffTime = 0; // 初始化关闭时间为0
// }

// void UvcontrolLyf::begin() {
//   FastLED.addLeds<WS2812, LED_PIN, GRB>(_leds, _numLeds).setCorrection(TypicalLEDStrip);
//   FastLED.setBrightness(_brightness);
//   pinMode(_switchPin, INPUT_PULLUP);
// }

// void UvcontrolLyf::update() {
//   int switchState = digitalRead(_switchPin);

//   if (switchState == LOW) {
//     if ((millis() - _lastDebounceTime) > _debounceDelay) {
//       _lastDebounceTime = millis();

//       if (!_ledState) {
//         _ledState = true;
//         _lastSwitchTime = millis();
//         for (int i = 0; i < _numLeds; i++) {
//           _leds[i] = CRGB::Purple;
//         }
//         FastLED.show();
//         delay(200);
//         // 设置关闭时间为当前时间加上15分钟
//         _turnOffTime = millis() + _ledOnDuration;
//       } else {
//         _ledState = false;
//         for (int i = 0; i < _numLeds; i++) {
//           _leds[i] = CRGB::Black;
//         }
//         FastLED.show();
//         delay(200);
//         // 关闭时清空关闭时间
//         _turnOffTime = 0;
//       }
//     }
//   }

//   // 如果当前时间大于关闭时间，则关闭灯
//   if (_turnOffTime != 0 && millis() > _turnOffTime) {
//     turnOff();
//   }
// }



// // 开启灯的函数
// void UvcontrolLyf::turnOn() {
//   // 设置 LED 状态为打开
//   _ledState = true;
//   // 记录当前时间
//   _lastSwitchTime = millis();
//   // 设置 LED 为紫色
//   for (int i = 0; i < _numLeds; i++) {
//     _leds[i] = CRGB::Purple;
//   }
//   // 更新 LED 显示
//   FastLED.show();
//   // 设置关闭时间为当前时间加上15分钟
//   _turnOffTime = millis() + _ledOnDuration;
// }

// // 关闭灯的函数
// void UvcontrolLyf::turnOff() {
//   // 设置 LED 状态为关闭
//   _ledState = false;
//   // 设置所有 LED 为黑色（关闭）
//   for (int i = 0; i < _numLeds; i++) {
//     _leds[i] = CRGB::Black;
//   }
//   // 更新 LED 显示
//   FastLED.show();
//   // 关闭时清空关闭时间
//   _turnOffTime = 0;
// }




// #include "uvControl.h"

// UvcontrolLyf::UvcontrolLyf() {
//   _ledPin = LED_PIN;
//   _switchPin = SWITCH_PIN;
//   _numLeds = NUM_LEDS;
//   _brightness = BRIGHTNESS;
//   _ledOnDuration = LED_ON_DURATION;

//   _leds = new CRGB[_numLeds];
//   _ledState = false;
//   _lastSwitchTime = 0;
//   _lastDebounceTime = 0;
//   _debounceDelay = 50;
//   _turnOffTime = 0;
//   _buttonPressed = false; // 初始化按钮状态为未按下
// }

// void UvcontrolLyf::begin() {
//   FastLED.addLeds<WS2812, LED_PIN, GRB>(_leds, _numLeds).setCorrection(TypicalLEDStrip);
//   FastLED.setBrightness(_brightness);
//   pinMode(_switchPin, INPUT_PULLUP);
// }

// void UvcontrolLyf::update() {
//   int switchState = digitalRead(_switchPin);

//   if (switchState == LOW) {
//     if (!_buttonPressed && (millis() - _lastDebounceTime) > _debounceDelay) {
//       _lastDebounceTime = millis();
//       _buttonPressed = true;

//       if (!_ledState) {
//         _ledState = true;
//         _lastSwitchTime = millis();
//         for (int i = 0; i < _numLeds; i++) {
//           _leds[i] = CRGB::Purple;
//         }
//         FastLED.show();
//         delay(200);
//         // 设置关闭时间为当前时间加上15分钟
//         _turnOffTime = millis() + _ledOnDuration;
//       } else {
//         _ledState = false;
//         for (int i = 0; i < _numLeds; i++) {
//           _leds[i] = CRGB::Black;
//         }
//         FastLED.show();
//         delay(200);
//         // 关闭时清空关闭时间
//         _turnOffTime = 0;
//       }
//     }
//   } else {
//     _buttonPressed = false; // 释放按钮时重置按钮状态
//   }

//   // 如果当前时间大于关闭时间，则关闭灯
//   if (_turnOffTime != 0 && millis() > _turnOffTime) {
//     turnOff();
//   }
// }

// void UvcontrolLyf::turnOn() {
//   // 设置 LED 状态为打开
//   _ledState = true;
//   // 记录当前时间
//   _lastSwitchTime = millis();
//   // 设置 LED 为紫色
//   for (int i = 0; i < _numLeds; i++) {
//     _leds[i] = CRGB::Purple;
//   }
//   // 更新 LED 显示
//   FastLED.show();
//   // 设置关闭时间为当前时间加上15分钟
//   _turnOffTime = millis() + _ledOnDuration;
// }

// void UvcontrolLyf::turnOff() {
//   // 设置 LED 状态为关闭
//   _ledState = false;
//   // 设置所有 LED 为黑色（关闭）
//   for (int i = 0; i < _numLeds; i++) {
//     _leds[i] = CRGB::Black;
//   }
//   // 更新 LED 显示
//   FastLED.show();
//   // 关闭时清空关闭时间
//   _turnOffTime = 0;
// }

// 

#include "uvControl.h"

UvcontrolLyf::UvcontrolLyf() {
  _ledPin = LED_PIN;
  _switchPin = SWITCH_PIN;
  _numLeds = NUM_LEDS;
  _brightness = BRIGHTNESS;
  _ledOnDuration = LED_ON_DURATION;

  _leds = new CRGB[_numLeds];
  _ledState = false;
  _lastSwitchTime = 0;
  _lastDebounceTime = 0;
  _debounceDelay = 50;
  _turnOffTime = 0;
  _buttonPressed = false;
  _isTimerSet = false;  // 初始化定时器标志为 false
}

void UvcontrolLyf::begin() {
  FastLED.addLeds<WS2812, LED_PIN, GRB>(_leds, _numLeds).setCorrection(TypicalLEDStrip);
  FastLED.setBrightness(_brightness);
  pinMode(_switchPin, INPUT_PULLUP);
}

void UvcontrolLyf::update() {
  int switchState = digitalRead(_switchPin);

  if (switchState == LOW) {
    if (!_buttonPressed && (millis() - _lastDebounceTime) > _debounceDelay) {
      _lastDebounceTime = millis();
      _buttonPressed = true;

      if (!_ledState) {
        turnOn();  // 调用 turnOn() 函数来打开 LED
      } else {
        turnOff();  // 调用 turnOff() 函数来关闭 LED
      }
    }
  } else {
    _buttonPressed = false;
  }

  if (_turnOffTime != 0 && millis() > _turnOffTime) {
    turnOff();
  }
}

void UvcontrolLyf::turnOn() {
  _ledState = true;
  _lastSwitchTime = millis();
  for (int i = 0; i < _numLeds; i++) {
    _leds[i] = CRGB::Purple;
  }
  FastLED.show();

  if (_isTimerSet && (millis() - _timerStartTime) >= _ledOnDuration) {
    turnOff();
  } else {
    _isTimerSet = true;
    _timerStartTime = millis();
    _turnOffTime = millis() + _ledOnDuration;
  }
}

void UvcontrolLyf::turnOff() {
  _ledState = false;
  for (int i = 0; i < _numLeds; i++) {
    _leds[i] = CRGB::Black;
  }
  FastLED.show();
  _turnOffTime = 0;
  _isTimerSet = false;  // 在关闭 LED 时重置定时器标志
}